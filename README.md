# id-dl
***

**Firstly i wanted to pay tribute to the work of the invidious team.
Without them this simple litle project would not have been possible.
Feel free to donate or contribute for supporting their great project.**
---

### What is id-dl ?

##### Another video downloader ?
<p>Yes but based on invidious only.
This tool won't do http requests out of their servers.</p>
<p>Really lightweight (a few lines), just python builtins, no dependencies.
A bit like pytube but using the invidious API.</p>

#### How does it work ?
1. A first get HTTP request is send to the API with the video id and the 3 fields wanted as params.
The api will return us 3 json objects after it:
	* title (of the video)
	* formatStreams (classicals videos options: mp4 with differents qualities and 3gp for the lowest quality)
	* adaptiveFormats (video without sound and audio only options: mp4/webm/m4a with differents qualities)

2. Depending of the args given to the app it will extract the good values from the api json response,
values needed as params for the post request to /download.

3. A post request will be made to /download with as params:
	* video_id
	* title
	* download_widget: itag, extension(mime-type)

4. /download request will redirect to the right url with rights params
to download the file. It should work straight forward but the choice is to
block the redirection to ensure the download will be made by an invidious server 'as a proxy'.

5. If there is a value given in the conf file (conf.ini) for the local_folder_for_dwnld,
the downloaded file will be saved in this folder, otherway default folder where to save are
~/Videos for videos and ~/Music for audio.
The name of the file will be: title.file_extension(mp4, 3gp, m4a, webm).
If the file already exists, the user will be asked if he wants to overwrite it.
If the choice to not overwrite the file is made, the program will print aborted and exit.

6. As the post request redirection is blocked, the tool will use the 'location' header value 
of this post request response. A last get http request will be made to the invidious server
with uri and params from this location field value to download the file requested by args.

#### Limitations
1. The high quality option is available only for videos without sound (video only and webm format without the audio only switch).
	* The reason why is pretty simple, the high quality formats for audios and regular videos are not free.
	So you'll get medium quality (720p) for regular videos and medium quality to for audio.
2. Depending of where you are located, maybe there isn't an invidious server 'close' to you
and you may experiment low download rate and timeout issue.

##### Choose the right invidious server
As documented in the conf.ini server field (eg. SERVER_URL):

	> You can use every server which don't need login
	> but with api, dwnld and chore enabled.
	> Try to find the 'nearest' to you
	> for better DL speed.
	> You can find a list at https://api.invidious.io/
	**The SERVER_URL conf value can't be blank**
	
Also if you have donwload issue(s) due to your internet connection quality,
and you get a timeout related error you can change the timeout value
of the conf.ini file to an upper value to fix it.

###### Maybe i'll add the option to login to an invidious server later

### How to use it ?
<p>The python executable is located at {gitFolder}/id_dl/id-dl.py</p>
<p>python id-dl.py -h should be a good start</p>
