"""lib for http part not api related"""
from typing import Dict
import sys
import http.client
import urllib.parse


def get_params_to_post(video_id: str, title: str,
                       itag: str, ext: str) -> Dict:
    """return the dict to post as param to /download"""
    download_widget: str = '{' + f'"itag":{itag},"ext":"{ext}"' + '}'
    return {'id': video_id, 'title': title, 'download_widget': download_widget}


def get_header_location(conn: object,
                        params_to_post) -> str:
    """Do a post request to /download and return the value of
    the header field Location"""
    params: str = urllib.parse.urlencode(params_to_post)
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}
    conn.request('POST', '/download', params, headers)
    resp = conn.getresponse()
    try:
        resp.url
    except AttributeError:
        location = resp.getheader('Location')
        if location.startswith('/'):
            conn.close()
            return location
    print('should investigate on this case, pls contact me')
    sys.exit()


def download_the_file(server_url: str,
                      uri: str,
                      timeout: int,
                      dest: str,
                      chunk_size: int) -> None:
    """Download the file and show it's download status"""
    def show_dl_status(dwnled_size) -> None:
        """Show the download status of the file downloaded"""
        steps: int = 25
        done = int(steps*dwnled_size/file_size)
        dwnled_size = min(dwnled_size, file_size)
        # sys.stdout and end='/r' for rewrite the same line
        #   every time it's called
        print(f"Download status: [{'█'*done}{(' '*(steps-done))}]"
              f" {dwnled_size}/{file_size} MiB",
              end='\r', file=sys.stdout, flush=True)

    conn = http.client.HTTPSConnection(server_url, timeout=timeout)
    conn.request('GET', uri)
    resp = conn.getresponse()
    file_size = round(float(resp.getheader('Content-Length'))/10**6, 1)
    with open(dest, 'wb') as file:
        i = 0
        while chunk := resp.read(chunk_size):
            file.write(chunk)
            show_dl_status(round(chunk_size*(i+1)/10**6, 1))
            i += 1

    # print a new line cause print is used to print to stdout with end="\r"
    # as this it won't break row display of the term
    print()
