"""lib of things related to invidious directly"""
from typing import Dict, List, Tuple
import http.client
import urllib.parse
import json
from colours import font_colours as FC


def get_json_infos(server_url, video_id, params, timeout) -> Tuple:
    """return the json objects requested in params as a Dict"""
    params: str = urllib.parse.urlencode(params)
    uri: str = f'/api/v1/videos/{video_id}?{params}'
    conn = http.client.HTTPSConnection(server_url, timeout=timeout)
    conn.request('GET', uri)
    resp: str = conn.getresponse()
    return conn, json.loads(resp.read())


def get_download_option(json_sources: Dict,
                        true_args: List) -> Tuple:
    """extract values from the json send by the invidious api
       for params to send later to /download;"""

    def get_itag_for_qual(json_sources: Dict, wanted_type: str,
                          quality_key: str, quality: str) -> str:
        """"Itag will be used later for requesting /download"""
        is_found = False
        for json_source in json_sources:
            type = json_source['type'].split(';')[0]
            if type == wanted_type and quality_key in json_source.keys():
                if json_source[quality_key] == quality:
                    is_found = True
                    itag = json_source['itag']
                    break
        assert is_found,\
            f'{FC.FAIL}'\
            'No source available with those args.\n'\
            'Try again with -HQ or -LQ or default (medium qual)'\
            f'{FC.ENDC}'
        return itag

    def get_itag_for_AO(json_sources: Dict, true_args: List,
                        extension: str) -> str:
        """Itag will be used later for requesting  /download
        Quality param and key differ from video only options"""
        quality_key = 'audioQuality'
        if 'high_quality' in true_args:
            itag = get_itag_for_qual(json_sources, extension,
                                     quality_key, 'AUDIO_QUALITY_HIGH')
        elif 'low_quality' in true_args:
            itag = get_itag_for_qual(json_sources, extension,
                                     quality_key, 'AUDIO_QUALITY_LOW')
        # default
        else:
            itag = get_itag_for_qual(json_sources, extension,
                                     quality_key, 'AUDIO_QUALITY_MEDIUM')
        return itag

    def get_itag_for_VO(json_sources: Dict, true_args: List,
                        extension: str) -> str:
        """Itag will be used later for requesting the file url.
           Quality param and key differ from audio only options"""
        quality_key = 'resolution'
        if 'high_quality' in true_args:
            itag = get_itag_for_qual(json_sources, extension,
                                     quality_key, '1080p')
        elif 'low_quality' in true_args:
            itag = get_itag_for_qual(json_sources, extension,
                                     quality_key, '360p')
        # default
        else:
            itag = get_itag_for_qual(json_sources, extension,
                                     quality_key, '720p')
        return itag

    if true_args:
        # webm format only available for audio only or video only
        # video with sound have their own quality param and key
        if 'webm_format' in true_args:
            if 'audio_only' in true_args:
                extension = 'audio/webm'
                itag = get_itag_for_AO(json_sources, true_args, extension)
            else:
                extension = 'video/webm'
                itag = get_itag_for_VO(json_sources, true_args, extension)
        elif 'audio_only' in true_args:
            extension = 'audio/mp4'
            itag = get_itag_for_AO(json_sources, true_args, extension)
        elif 'video_only' in true_args:
            extension = 'video/mp4'
            itag = get_itag_for_VO(json_sources, true_args, extension)
        elif 'low_quality' in true_args:
            extension = 'video/3gpp'
            itag = get_itag_for_qual(json_sources, extension,
                                     'quality', 'small')
    else:
        for json_source in json_sources:
            type = json_source['type'].split(';')[0]
            if type == 'video/mp4' and json_source['quality'] == 'hd720':
                itag = json_source['itag']
                extension = type
                break
    return itag, extension
