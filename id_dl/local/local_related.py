"""lib of things not related to invidious directly"""
import sys
import argparse
import configparser
from pathlib import Path
from typing import Dict, List
from colours import font_colours as FC


content_type_to_ext = {
    'audio/mp4': 'm4a',
    'audio/webm': 'webm',
    'video/mp4': 'mp4',
    'video/webm': 'webm',
    'video/3gpp': '3gp'
    }


def load_conf() -> Dict:
    """use configparser to load the conf of conf.ini folder"""
    app_folder = f"{__file__.rsplit('/', 1)[0]}/../../"
    conf_file_path = f'{app_folder}/conf.ini'
    default_conf =\
        """
        [DEFAULT]
        # folder of where to store downloaded files
        # if blank the default will be:
        # 	~/Videos for videos and ~/Music for audio only
        # have to exist or the app will throw a key exception
        local_folder_for_dwnld =
        # u can use every server without login
        # but with api dwnl and chore
        # try to find the closest for better DL speed
        # u can find a list at https://api.invidious.io/
        # can't be blank
        SERVER_URL = https://yt.artemislena.eu
        """
    config = configparser.ConfigParser()
    config.read(conf_file_path)
    app_conf = (config['DEFAULT'])
    assert len(app_conf) >= 1,\
        f'{FC.FAIL}'\
        'The content of the \033[0;31mconf.ini file\033[0m '\
        'at the root of this git folder should be as exemple:\n'\
        f'{default_conf}\n'\
        f'\033[0;31mMissing conf values\033[0m:\n {app_conf}'\
        f'{FC.ENDC}'
    return app_conf


def get_args() -> object:
    """use argparse to obtain the app cmd line args"""
    # todo: make this part cleaner with configparser options if possible
    description = 'Download videos from the invidious server specified in config file.'
    example = 'example: python inv-dl.py Zxyb9eZGU40'\
              ' for DL this video with defaults for quality'\
              ' and format (mp4 or m4a if audio only)'
    parser = argparse.ArgumentParser(description=description,
                                     epilog=example)
    help = 'The value of the v param'\
           ' from the url of the video to DL\n'\
           ' https://yt.artemislena.eu/watch?v=v_param_value'
    parser.add_argument('v_param_value', type=str, nargs=1,
                        help=help)
    help = 'Video without sound (could be useful :))'
    parser.add_argument('-VO', '--video-only', action='store_true', help=help)
    parser.add_argument('-AO', '--audio-only', action='store_true')
    parser.add_argument('-HQ', '--high-quality', action='store_true')
    parser.add_argument('-LQ', '--low-quality', action='store_true')
    help = 'Video with sound unavailable in this format.\n'\
           'Default is video only.'
    parser.add_argument('-WM', '--webm-format', action='store_true', help=help)
    args = parser.parse_args()
    if args.high_quality:
        assert args.video_only or args.webm_format and not args.audio_only,\
            f'{FC.FAIL}'\
            'High quality is just available for free for video only formats'\
            f'{FC.ENDC}'
    if args.low_quality and args.high_quality:
        assert False,\
            f'{FC.FAIL}'\
            '-HQ + -LQ not compatible'\
            f'{FC.ENDC}'
    if args.video_only and args.audio_only:
        assert False,\
            f'{FC.FAIL}'\
            '-VO + -AO not implemented'\
            f'{FC.ENDC}'
    if args.webm_format:
        assert not args.video_only,\
            f'{FC.FAIL}'\
            'Default for webm is video only.\n'\
            'Try again without -VO'\
            f'{FC.ENDC}'
    return args


def get_true_args(args) -> List:
    """return a list of args equal to true"""
    true_args: List = []
    for key, val in vars(args).items():
        if val is True:
            true_args.append(key)
    return true_args


def get_path_to_store_dl(lffd: str, title: str, extension: str) -> str:
    """return the path of the place where DL will be stored
       or exit if the file already exists and the user does not
       want to overwrite it.
    """

    def get_local_folder_for_dl(conf_path: str, is_audio: bool) -> str:
        """return the path of the place where DL will be stored"""
        if conf_path.strip():
            local_folder_for_dwnld: str =\
                conf_path.strip()
        else:
            if is_audio:
                local_folder_for_dwnld: str = f'{Path.home()}/Music/'
            else:
                local_folder_for_dwnld: str = f'{Path.home()}/Videos/'
        return local_folder_for_dwnld

    # get the path where the file will be stored
    #   in case this path isn't set in conf.ini file:
    #   the default will be ~/Videos for videos and ~/Music for audio only
    if 'audio' in extension:
        local_folder_for_dl = get_local_folder_for_dl(lffd,
                                                      is_audio=True)
    else:
        local_folder_for_dl = get_local_folder_for_dl(lffd,
                                                      is_audio=False)
    # get the file extension .mp4 as example
    file_extension = content_type_to_ext[extension]
    # build the full path of where to store the file to donwload
    path_to_file = f'{local_folder_for_dl}{title}.{file_extension}'
    path = Path(path_to_file)
    if path.is_file():
        resp = input('File already exists, do you want to overwrite it ?'
                     ' [N\\y]: ').strip().lower()
        if not resp == 'y':
            print(f'{FC.INFOGREEN}Aborted{FC.ENDC}')
            sys.exit()
    return path_to_file
