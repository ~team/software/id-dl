#!/usr/bin/python3
"""A tool for DL video with or without sound, or just audio
from invidious servers without auth
but with api core and dwnld enabled"""

if __name__ == '__main__':
    from banner import banner
    from colours import font_colours as FC
    from local import local_related as LR
    from HTTP.API import api_related as AR
    from HTTP.NOT_API import not_api_related as NAR
    from typing import Dict, List, Tuple

    print(banner.banner)
    app_conf: Dict = LR.load_conf()
    # timeout param for requests requests
    TIMEOUT = int(app_conf['TIMEOUT'])
    # the invidious sever url
    SERVER_URL = app_conf['SERVER_URL'].strip('https://').strip()
    # folder where to download files
    LFFD = app_conf['local_folder_for_dwnld']
    del app_conf
    args = LR.get_args()
    video_id: str = args.v_param_value[0]
    del args.v_param_value
    # json objects about title and formats available.
    params: Tuple = [('fields', 'title,formatStreams,adaptiveFormats')]
    # get HTTP request to api for infos about the video
    conn, json_infos = AR.get_json_infos(SERVER_URL, video_id, params, TIMEOUT)
    print(f'[{FC.OKGREEN}+{FC.ENDC}]'
          f' Invidious API {FC.OKGREEN}reached{FC.ENDC}')
    title: str = json_infos['title'].strip("\00").replace('/', '|')
    del json_infos['title']
    true_args: List = LR.get_true_args(args)
    del args
    if 'video_only' not in true_args and\
            'audio_only' not in true_args and\
            'webm_format' not in true_args:
        # formatStreams is for videos with sound (Default)
        json_sources = json_infos['formatStreams']
        del json_infos['adaptiveFormats']
    else:
        # adaptiveFormats is for audio or video without sound (AO, VO)
        json_sources = json_infos['adaptiveFormats']
        del json_infos['formatStreams']
    # get params to post to /download (strings)
    itag, extension = AR.get_download_option(json_sources,
                                             true_args)
    # return the path where to store the DL
    #   if allready exists, asks the user to confirm the overwrite of this file
    #   or exit
    dest: str = LR.get_path_to_store_dl(LFFD, title, extension)
    # get the formated dict of params to attach
    # to the future post request to /download
    params_to_post: Dict = NAR.get_params_to_post(video_id, title,
                                                  itag, extension)
    # do the post request to /download and
    # return the header location field value
    uri = NAR.get_header_location(conn,
                                  params_to_post)
    mess = f'Downloading {FC.INFOGREEN}'\
           f'{title}{FC.ENDC}\n'\
           '     '\
           f'to {FC.INFOMAGENTA}{dest}{FC.ENDC}'
    print(f'[{FC.OKGREEN}+{FC.ENDC}] {mess}')
    CHUNK_SIZE: int = 1024*150
    # download the file and show the DL status
    NAR.download_the_file(SERVER_URL,
                          uri,
                          TIMEOUT,
                          dest,
                          CHUNK_SIZE)
